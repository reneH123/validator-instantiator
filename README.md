 COPYRIGHT notification
Author: René Haberland,
  2007, 2008, 30.12.2018, 2020 Saint Petersburg, Russia

All software is licensed under Creative Commons 4.0 Share Alike (CC BY-SA 4.0), for more details refer to https://creativecommons.org/licenses/by-sa/4.0/legalcode.


3RD PARTY SOFTWARE USING:
 - Haskell (Glasgow Haskell Compiler, GHC/GHCI)
 - Haskell libraries: HXT, others)
 - regular expression tutorial (Haskell code adaptation, originally from Simon P. Jones, Kent University, UK)



This software is related to:

@misc{haberl2019unification,
    title={Unification of Template-Expansion and XML-Validation},
    author={René Haberland},
    year={2019},
    eprint={1906.08369},
    archivePrefix={arXiv},
    primaryClass={cs.LO}
}


@misc{haberl2019narrowing,
    title={Narrowing Down XML Template Expansion and Schema Validation},
    author={René Haberland},
    year={2019},
    eprint={1912.10816},
    archivePrefix={arXiv},
    primaryClass={cs.LO}
}
